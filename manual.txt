# Vytvorit novu branch z develop
git checkout -b feature/test

# urobime nejake zmeny
...

# Kontrola co je nove
git status

# Pridat vsetky zmeny do stagu
git add .

# Alebo pridat jednotlivo zmeny do stagu
git add manual.txt

# Pomocou git push, git pull a git fetch komunikujeme so serverom
# git pull a git push su vramci jedneho branchu
# git fetch je pre cely repozitar
